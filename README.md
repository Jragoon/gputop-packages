# Package for prebuilt GPU Top with web-ui

While the files are large, it configures everything while allowing the user to not have to download the necessary tools (such as emscripten)
to build gputop from source.

# For Debian:

```
user $ cd gputop-deb-package
root # dpkg -i gputop-webui_1.1_amd64.deb
```
To remove the package, run:

```
root # dpkg -r gputop-webui
```

# For RPM:

```
user $ cd gputop-packages
root # rpm -i gputop-1.0-x86_64.rpm
```
To remove the package, run:

```
root # rpm -e gputop
```
# Running the web-ui


Make sure that libssl-dev is installed, it is needed in order to run gputop:

**Debian: libssl-dev**

**RPM: libopenssl-devel**

After the package is installed, to run the web-ui:

```
user $ cd ~/gputop/ui
user $ index.html
```

Happy profiling!

Original source: https://github.com/rib/gputop (helpful runtime instructions and wiki)

